## AUTH0
```
npm i
node ./src/server.js
```

****

1) Haga una petición tipo POST a http://localhost:3000/api/login con los headers: 'Content-Type': 'application/json' junto con un objeto en el body: 
```
{
	"username": "USERNAME",
	"password": "PASSWORD"
}
```

1) Haga una petición tipo POST a http://localhost:3000/api/login con los headers: 'Content-Type': 'application/json' junto con un objeto en el body: 
```
{
	"username": "USERNAME",
}
```

****

Cuando se hace la verificación de la existencia del usuario se instruye el guardado de los datos en una tabla de mongodb con los siguientes campos: 
```
{
    email: { type: String, trim: true, index: true, unique: true },
    email_verified: { type: Boolean, default: true },
    username: { type: String, trim: true, index: true, unique: true },
    user_metadata: { first_login: { type: Boolean } }
};
```

Cuando se hace una petición al endpoint /api/login se instruye el guardado de los datos de la petición en una tabla de mongodb con los siguientes campos:
```
{
    url: { type: String, trim: true },
    registrerDate: { type: Date, default: Date.now },
    method: { type: String, trim: true },
    data: { type: Schema.Types.Mixed },
    headers: { type: Schema.Types.Mixed }
};
```