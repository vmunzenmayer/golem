/*jshint esversion: 6 */
/* jshint node: true */

const request = require('request');
const Services = require('./../Services');
const Constant = require('../../config.json');
let infoValidUser = null;

exports.authAPP = function (req, callback) {
  const appAuthParams = {
    url: `https://${Constant.AUTH0_CUSTOM_DOMAIN}/oauth/token`,
    json: true,
    body: {
      audience: Constant.AUTH0_APP_AUDIENCE,
      client_id: Constant.AUTH0_APP_CLIENT_ID,
      grant_type: Constant.AUTH0_APP_GRANT_TYPE,
      client_secret: Constant.AUTH0_APP_CLIENT_SECRET,
    },
    headers: {
      'Content-Type': 'application/json',
    },
  };

  request.post(appAuthParams, function (error, response, body) {
    if (error) {
      console.error('APP_AUTH_ERROR', error);
      callback(error);
    } else {
      console.log('firstBody', body, 'firstBody');
      isUser(req, body, callback);
    }
  });
};
const isUser = function (req, body, callback) {

  const userAuthParams = {
    url: `https://${Constant.AUTH0_CUSTOM_DOMAIN}/api/v2/users?q=username:"${req.username}"&search_engine=v3`,
    json: true,
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${body.access_token}`,
    },
  };
  request.get(userAuthParams, function (error, response, body) {
    if (error) {
      console.error('USER_AUTH_ERROR', error);
      callback(error);
    } else {
      const bodyItem = body[0];
      infoValidUser = bodyItem;
      const saveInDB = {
        email: bodyItem.email,
        email_verified: bodyItem.email_verified,
        username: bodyItem.username,
        user_metadata: { first_login: bodyItem.user_metadata.first_login }
      };
      /* console.info('USER_AUTH_RESPONSE', body); */

      Services.userServices.findUser({username: bodyItem.username}, null,  (err, resp) => {
        if (err) throw new Error(err);
        if (resp == null) {
          Services.userServices.createuser(saveInDB, (err, resp) => {
            if (err) throw new Error(err);
            /* console.info(resp, 'Usuario creado en DB'); */
          });
        }
      });
      callback(undefined, body[0], { 'userExist': true });
    }
  });
};

exports.authUser = function(req, callback) {
  const userLoginParams = {
    url: `https://${Constant.AUTH0_CUSTOM_DOMAIN}/oauth/token`,
    headers: {
      'content-type': 'application/x-www-form-urlencoded',
    },
    form: {
      grant_type: Constant.AUTH0_CUSTOM_GRANT_TYPE,
      username: req.username,
      password: req.password,
      audience: Constant.AUTH0_CUSTOM_AUDIENCE,
      scope: Constant.AUTH0_CUSTOM_SCOPE,
      client_id: Constant.AUTH0_CUSTOM_CLIENT_ID,
      realm: Constant.AUTH0_CUSTOM_REALM
    }
  };
  request.post(userLoginParams, function (error, response, body) {
    if (error) {
      console.error('USER_LOGIN_ERROR', error);
      callback(error);
    } else {
      /* console.info('USER_LOGIN_SUCCESS', body); */
      callback(undefined, JSON.parse(body), { 'userIsValid': true});
    }
  });
};
